<aside class="main-sidebar sidebar-light-primary elevation-2">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <img src="{{asset('/adminlte/img/AdminLTELogo.png')}}" alt="Logo Warcod" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">WarCod</span>
  </a>
<div class="sidebar">
    <!-- SidebarSearch Form -->
    <div class="form-inline mt-2 justify-content-center">
      
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/" class="nav-link {{ ($page=='Home')? 'active': '' }}">
            <i class="nav-icon fas fa-home"></i>
            <p>
              Home
            </p>
          </a>
          <li class="nav-item">
            <a href="/list" class="nav-link {{ ($page=='List of Categories')? 'active': '' }}">
              <i class="nav-icon fas fa-bookmark"></i>
              <p>
                Category
              </p>
            </a>
          </li>
          <span></span>
          <li class="nav-item">
            <a href="/list/{{ Auth::id() }}" class="nav-link {{ ($page=='My Categories')? 'active': '' }}">
              <i class="nav-icon far fa-bookmark"></i>
              <p>
                My Category
              </p>
            </a>
          </li>
        </li>
        
    </nav>
    <!-- /.sidebar-menu -->
  </div>
</aside>