<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        {{-- <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a> --}}
      </li>
      
    </ul>
    
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      
      <!-- Navbar Search -->
      <li class="nav-item">
        <form class="form-inline my-2 my-lg-0" method="GET" action="/search">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search">
          <button class="btn btn-outline-primary my-2 my-sm-0 btn-sm mx-2" type="submit">Search</button>
        </form>
      </li> 
      <li class="nav-item">
        <a href="{{ route('question.create') }}" class="btn btn-success btn-md mr-2">
            <i class="fas fa-plus"></i> Add Question
        </a>
      </li>
      <!-- Messages Dropdown Menu -->
      
      <!-- Notifications Dropdown Menu -->
     
      <li class="nav-item dropdown user-menu">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
          <span class="d-none d-md-inline">
            {{ Auth::user()->email}}
          </span>
          <i class="fas fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
          {{-- <!-- User image -->
          <li class="user-header bg-primary">
            <p>
              {{ Auth::user()->name}}
              <small>Member</small>
            </p>
          </li>
          <!-- Menu Body --> --}}
          <!-- Menu Footer-->
          <li class="user-footer">
            <a href="{{ route('profile') }}" class="btn btn-primary btn-flat">Profile</a>
            <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"
            class="btn btn-danger btn-flat float-right">
            Sign out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
          </li>
        </ul>
      </li>
      
    </ul>
  </nav>