@extends('layout.login')
@section('header')
    Register Member
@endsection
@section('content')
<form action="{{ route('register') }}" method="post">
  @csrf
      <div class="form-group">
          <label>Nama</label>
          <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
      </div>

      <div class="form-group">
          <label>Email</label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
      </div>
      <div class="form-group">
        <label>Password</label>
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
          @error('password')
          <br>
              <span class="alert alert-danger mt-" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
    </div>
    <div class="form-group">
        <label>Konfirmasi Password</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input id="umur" type="number" class="form-control" name="umur" required>
    </div>
    <div class="form-group">
        <label>Biodata</label>
        <textarea class="form-control" name="bio" required></textarea>
    </div>
    <div class="form-group">
        <label>Alamat</label>
        <textarea class="form-control" name="alamat" required></textarea>
    </div>
      <button type="submit" class="btn btn-primary btn-block">Register</button>
      <hr>
      <p class="text-center">Sudah punya akun? <a href="{{ route('login') }}">Login</a> sekarang!</p>
  </form>
@endsection