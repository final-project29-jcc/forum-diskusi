@extends('layout.login')
@section('header')
    Login Warcod
@endsection
@section('content')
<form action="{{ route('login') }}" method="post">
  @csrf
      <div class="form-group">
          <label>Email</label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
            <br>
            <span class="alert alert-danger mt-4" role="alert">
                 <strong>{{ $message }}</strong>
            </span>
            @enderror
      </div>
      <div class="form-group">
          <label>Password</label>
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            @error('password')
            <br>
                <span class="alert alert-danger mt-" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
      </div>
      <button type="submit" class="btn btn-primary btn-block">Log In</button>
      <hr>
      <p class="text-center">Belum punya akun? <a href="{{ route('register') }}">Register</a> sekarang!</p>
  </form>
@endsection