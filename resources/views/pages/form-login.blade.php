@extends('layout.login')
@section('header')
    Login
@endsection
@section('content')
<form action="{{ route('home') }}" method="post">
  @csrf
      <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" class="form-control" placeholder="Email" required="">
      </div>
      <div class="form-group">
          <label>Password</label>
          <input type="password" name="password" class="form-control" placeholder="Password" required="">
      </div>
      <button type="submit" class="btn btn-primary btn-block">Log In</button>
      <hr>
      <p class="text-center">Belum punya akun? <a href="{{ route('register') }}">Register</a> sekarang!</p>
  </form>
@endsection