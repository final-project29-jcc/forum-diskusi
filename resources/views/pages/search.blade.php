@extends('layout.main')
@section('header')
  {{ $page }}
@endsection
@section('content')
<table id="example2" class="table table-bordered table-hover">
                  <tbody>
                  @forelse ($questions as $key => $question)
                  <tr>
                    <td class="col-lg-11 border-0">
                    <a href="/question/show/{{$question->id}}"><h4>{{ $question->judul }}</h4></a>
                    <p><small>{!! Str::limit($question->content, 50) !!}</small></p>
                    <p><small>Dikirim oleh <b><a href="">{{ $question->user->name }}</a></b></small></p>
                    <!--<p style="color:green">20 Balasan</p>-->
                    </td>
                  </tr>  
                  @empty
                      Oops! Pertanyaan tidak ditemukan :( Coba kata kunci lainnya!
                  @endforelse
                  <tbody>
          </table>
          <div class="mt-2">{{$questions->links()}}</div>        
@endsection
