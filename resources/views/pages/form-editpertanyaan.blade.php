@extends('layout.main')

@section('header')
    Edit Pertanyaan
@endsection


@section('content')
    <form method="POST" action="/question/update">
    @csrf

    <div class="form-group">
        <label>Judul Pertanyan <span style="color:red">*</span></label>
        <input type="hidden" name="users_id" value="{{Auth::user()->id}}">
        <input type="hidden" name="id" value="{{$question->id}}">
        <input type="text" value="{{ $question->judul }}" class="form-control" placeholder="Masukkan judul pertanyaan" name="judul" required>
    </div>
    <div class="form-group">
        <label>Isi Pertanyaan</label>
        <textarea class=" my-editor form-control" rows="5" name="content">{!!$question->content!!}</textarea>
    </div>
    <div class="form-group">
        <label>Pilih Kategori <span style="color:red">*</span></label>
        <select name="categories_id" class="form-control">
            <option value="{{ $pilihan->id }}" hidden>{{ $pilihan->category }}</option>
            @forelse ($categories as $key => $category)
            <option value="{{$category->id}}">{{$category->category}}</option>
            @empty
            Tidak ada kategori
            @endforelse
            
            
        </select>
    </div>
    <div class="form-group">
        
        <input type="submit" class="btn btn-primary">
    </div>

    </form>
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/hqa1vlbrl9vr5jgq4u9tqnx72xzagcgkj1dzoguwfffmxvtm/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea.my-editor',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media image code mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter link image pageembed permanentpen table',
      /* enable title field in the Image dialog*/
      image_title: true,
        /* enable automatic uploads of images represented by blob or data URIs*/
        automatic_uploads: true,
        /*
            URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
            images_upload_url: 'postAcceptor.php',
            here we add custom filepicker only to Image dialog
        */
        file_picker_types: 'image',
        /* and here's our custom image picker*/
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            /*
            Note: In modern browsers input[type="file"] is functional without
            even adding it to the DOM, but that might not be the case in some older
            or quirky browsers like IE, so you might want to add it to the DOM
            just in case, and visually hide it. And do not forget do remove it
            once you do not need it anymore.
            */

            input.onchange = function () {
            var file = this.files[0];

            var reader = new FileReader();
            reader.onload = function () {
                /*
                Note: Now we need to register the blob in TinyMCEs image blob
                registry. In the next release this part hopefully won't be
                necessary, as we are looking to handle it internally.
                */
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);

                /* call the callback and populate the Title field with the file name */
                cb(blobInfo.blobUri(), { title: file.name });
            };
            reader.readAsDataURL(file);
            };

            input.click();
        },
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });
  </script>
@endpush