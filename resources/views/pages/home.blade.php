@extends('layout.main2')
@section('header')
    Home
@endsection
@section('content')
<div class="row mr-2 ml-2">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-header">
                <h5>Category</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                                      <tbody>
                                @forelse($categories as $key => $category)
                                      <tr>
                                        <td class="col-lg-7 align-middle align-center border-0">
                                          <a href="/categories/{{ $category->id }}">
                                            <h4>{{ $category->category}}</h4>
                                          </a>
                                          <p>{{ $category->description }}</p>
                                        </td>
                                      </tr>
                                      @empty
                                      <p class="text-center"> No category yet </p>
                                @endforelse
                    </table>
                    <a href="/list" class="btn btn-primary d-block">See Full Category</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                                      <tbody>
                                @forelse($question as $key => $question)
                                      <tr>
                                        <td class="col-lg-7 align-middle align-center border-0">
                                          <a href="/question/show/{{ $question->id }}">
                                            <h4>{{ $question->judul}}</h4>
                                          </a>
                                          <p>Dikirim oleh <b>{{$question->user->name}}</b>
                                            di Kategori <b>{{$question->category->category}}</b>
                                            pada {{$question->date}} {{$question->time}}
                                          </p>
                                          <p>
                                            {!!$question->content!!}
                                          </p>
                                        </td>
                                      </tr>
                                      @empty
                                      <p class="text-center"> No question yet </p>
                                @endforelse
                    </table>
        @guest
            h1
        @endguest
            </div>
        </div>
    </div>
</div>
@endsection