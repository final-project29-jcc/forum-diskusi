@extends('layout.main')

@section('header')
<a href="/categories/{{$question->category->id}}">{{$question->category->category}}</a>/Question
@endsection
@section('content')
        <h3>{{$question->judul}}</h3>
        <p>Dikirim oleh <b>{{$question->user->name}}</b>
            di Kategori <b>{{$question->category->category}}</b>
            pada {{$question->date}} {{$question->time}}
        </p>
        <p>
            {!!$question->content!!}
        </p>

<hr>
<h4 class="mt-4">Beri Balasan</h4>
<form class="form-group" action="/jawaban" method="post">
@csrf 
<div class="form-group">
    <input type="hidden" name="users_id" value="{{Auth::user()->id}}">
    <input type="hidden" name="questions_id" value="{{$question->id}}">
    <textarea class="my-editor form-control" rows="5" name="content"></textarea>
    
</div>
<div class="form-group">
        
    <input type="submit" class="btn btn-primary">
</div>
</form>
<!--pengulangan dari sini-->

@forelse ($question->jawaban as $item)
<table class="table table-bordered mb-2">
    <tr>
        <td class="col-lg-10 border-0">
        <b><span style="font-size:10pt;">{{$item->user->name}}<span></b> on {{$item->date}} {{$item->time}}

            <!--jika komentar milik user yang login-->
             
            <p>
            {!!$item->content!!}
            
        </td>
        @if ( $item->user->id===Auth::user()->id)
        <td class="border-0">     
        <form method="post" class="inline-form" style="float: inline-end;" action="{{ route('jawaban.destroy',$item->id) }}">
            @csrf
            @method('DELETE')
            <button type="button" class="btn btn-tool" data-toggle="modal" data-target="#exampleModal{{ $item->id }}"><i class="fas fa-pen"></i></button>
            <input type="hidden" name="questions_id" value="{{$item->questions_id}}">
            <button type="submit" class="btn btn-tool" title="Hapus">
              <i class="fas fa-times"></i>
            </button> 
        </form>
    </td>
 <!-- Modal -->
 <div class= "modal fade" id="exampleModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Jawaban</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="/jawaban/update/{{ $item->id }}" method="post">
            @csrf
            <input type="hidden" name="question_id" value="{{ $question->id }}">
            <div class="form-group">
              <input type="hidden" name="questions_id" value="{{$item->questions_id}}">
              <label for="message-text" class="col-form-label">Jawaban:</label>
              <textarea class=" my-editor form-control" name="content" id="message-text">{{ $item->content }}</textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</div>
  </div>
    @else
        
    @endif
</p>    
    </tr>
    </table>   
@empty
<h2>Belum ada jawaban</h2> 
@endforelse


@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/hqa1vlbrl9vr5jgq4u9tqnx72xzagcgkj1dzoguwfffmxvtm/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea.my-editor',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media image code mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      /* enable title field in the Image dialog*/
      image_title: true,
        /* enable automatic uploads of images represented by blob or data URIs*/
        automatic_uploads: true,
        /*
            URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
            images_upload_url: 'postAcceptor.php',
            here we add custom filepicker only to Image dialog
        */
        file_picker_types: 'image',
        /* and here's our custom image picker*/
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            /*
            Note: In modern browsers input[type="file"] is functional without
            even adding it to the DOM, but that might not be the case in some older
            or quirky browsers like IE, so you might want to add it to the DOM
            just in case, and visually hide it. And do not forget do remove it
            once you do not need it anymore.
            */

            input.onchange = function () {
            var file = this.files[0];

            var reader = new FileReader();
            reader.onload = function () {
                /*
                Note: Now we need to register the blob in TinyMCEs image blob
                registry. In the next release this part hopefully won't be
                necessary, as we are looking to handle it internally.
                */
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);

                /* call the callback and populate the Title field with the file name */
                cb(blobInfo.blobUri(), { title: file.name });
            };
            reader.readAsDataURL(file);
            };

            input.click();
        },
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter link image pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });

//    $('#exampleModal').on('show.bs.modal', function (event) {
//   var button = $(event.relatedTarget) // Button that triggered the modal
//   var recipient = button.data('id') // Extract info from data-* attributes
//   var recipient2 = button.data('content') // Extract info from data-* attributes
//   // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//   // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//   var modal = $(this)
//   modal.find('.modal-body input').val(recipient)
//   modal.find('.modal-body textarea').val(recipient2)
//     })
  </script>
@endpush