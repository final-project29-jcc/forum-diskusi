@extends('layout.main')
@section('header')
    Edit Categorise
@endsection
@section('content')
<form method="POST" action="/categories/update">
    @csrf

    <div class="form-group">
        <label>Nama Kategori <span style="color:red">*</span></label>
        <input type="hidden" value="{{ Auth::user()->id}}" name="users_id">
        <input type="hidden" value="{{ $categories->id}}" name="id">
        <input type="text" class="form-control" value="{{ $categories->category }}" placeholder="Masukkan nama kategori" name="category" required>
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea class="form-control" rows="5" name="description">{{ $categories->description }}</textarea>
    </div>
</div>
<div class="modal-footer">
  <input type="submit" class="btn btn-primary">
</div>
  </form>
@endsection