@extends('layout.main')
@section('header')
    Profile
@endsection
@section('content')
@if ($message = Session::get('success-upload'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif
@if ($message = Session::get('success-update'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif
@if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="d-flex justify-content-center">
    <div class="col-4">
        <div class="card-body">
            <form method="post" action="{{ route('upload') }}" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <div class="d-flex justify-content-center">
                    <img class="rounded-circle border border-dark" src="{{ asset('img/profile_picture/'.$profile->foto) }}" width="auto" height="200px" alt="{{ $profile->foto }}">
                </div>
                <div class="d-flex justify-content-center mt-5">
                    <input type="file" name="image" class="form-control">
                    <button type="submit" class="btn btn-primary btn-sm">
                        Upload
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-8">
        <div class="card-body">
            <form method="POST" action="{{ route('update') }}">
                @method('patch')
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $user->email) }}" autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-4">
                    <div class="col-md-6 offset-md-4">
                        <a href="{{ route('user.password.edit') }}" class="btn btn-primary">Ubah Password</a>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="umur" class="col-md-4 col-form-label text-md-right">{{ __('Umur') }}</label>

                    <div class="col-md-6">
                        <input id="umur" type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{ old('umur', $profile->umur) }}" autocomplete="umur">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="bio" class="col-md-4 col-form-label text-md-right">{{ __('Bio') }}</label>

                    <div class="col-md-6">
                        <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" required>{{ old('bio', $profile->bio) }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

                    <div class="col-md-6">
                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" required>{{ old('alamat', $profile->alamat) }}</textarea>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Update Profile
                        </button>
                    </div>
                </div>
            </form>
        </div>     
    </div>  
</div>
@endsection