@extends('layout.main')
@section('header')
  Questions - {{ $page }}
@endsection
@section('content')
<table id="example2" class="table table-bordered table-hover">
                  <tbody>
                  @forelse ($questions as $key => $question)
                  <tr class="border">
                    <td class="col-lg-10 border-0">
                    <a href="/question/show/{{$question->id}}"><h4>{{ $question->judul }}</h4></a>
                    <p>{!!$question->content!!}</p>
                    <p><small>Dikirim oleh <b><strong>{{ $question->user->name }}</strong></b></small></p>
                    <p style="color:green">{{ $jawaban->where('questions_id', $question->id)->count() }} Balasan</p>
                    </td>
                    @if ( $question->user->id===Auth::user()->id)
                    <td class="col-lg-10 border-0">     
                
                  <form method="post" class="inline-form" style="float: inline-end;" action="{{ route('question.destroy',$question->id) }}">
                        @csrf
                        @method('DELETE')
                <a href="/question/edit/{{ $question->id }}">
                    <button type="button" class="btn btn-tool" title="Edit">
                    <i class="fas fa-pen"></i>
                    </button>
                </a>
                <button type="submit" class="btn btn-tool" title="Hapus">
                  <i class="fas fa-times"></i>
                </button> 
                
                    </form>
                </td>
                @else
                    
                @endif
                  </tr>  
                  @empty
                      Oops! Belum ada pertanyaan di kategori ini. Jadilah yang pertama!
                  @endforelse
                  <tbody>
          </table>
          <div class="mt-2">{{$questions->links()}}</div>        
@endsection

