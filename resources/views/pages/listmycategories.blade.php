@extends('layout.main')
@section('header')
  My Categories
@endsection
@section('content')
<button type="button" data-toggle="modal" data-target="#catModal" class="btn btn-info btn-md mb-4" tabindex="-1" aria-hidden="true"><i class="fas fa-plus"></i> Add Category</button>
<div class="modal fade" id="catModal" role="dialog" arialabelledby="modalLabel" area-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Add Category</h5>
      </div>
      <div class="modal-body">
        <form method="POST" action="/categories/store">
          @csrf

          <div class="form-group">
              <label>Nama Kategori <span style="color:red">*</span></label>
              <input type="hidden" value="{{ Auth::user()->id}}" name="users_id">
              <input type="text" class="form-control" placeholder="Masukkan judul pertanyaan" name="name" required>
          </div>
          <div class="form-group">
              <label>Deskripsi</label>
              <textarea class="form-control" rows="5" name="description"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary">
      </div>
        </form>
    </div>
  </div>
</div>
<div class="table-responsive">
<table id="example1" class="table table-bordered table-hover">
                  <tbody>
            @forelse($categories as $key => $category)
                  <tr class="border">
                    <td class="col-lg-10 align-middle align-center border-0">
                      <a href="/categories/{{ $category->id }}">
                        <h4>{{ $category->category}}</h4>
                      </a>
                      <p>{{ $category->description }}</p>
                    </td>
                    
                    <td class="col-lg-2 align-middle align-center border-0">
                      <!--<p> <a href="" class="btn btn-sm btn-primary">21</a> Pertanyaan</p>-->
                      
                      <form method="post" class="inline-form" style="float: inline-end;" action="{{ route('categories.destroy',$category->id) }}">
                        @csrf
                        @method('DELETE')
                <a href="/categories/edit/{{ $category->id }}">
                    <button type="button" class="btn btn-tool" title="Edit">
                    <i class="fas fa-pen"></i>
                    </button>
                </a>
                <button type="submit" class="btn btn-tool" title="Hapus">
                  <i class="fas fa-times"></i>
                </button> 
                
                    </form>

                    </td>
                  </tr>
                  @empty
                  Kamu belum membuat Kategori!
            @endforelse
</table>

<div align="center">{{ $categories->links() }}</div>
</div>
@endsection

