@extends('layout.main')
@section('header')
    Ubah Password
@endsection
@section('content')
<center>
    <div class="col-lg-5">
    @if ($message = Session::get('tidak-update'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
    </div>
    @endif
    <form method="POST" action="{{ route('user.password.update') }}">
        @method('patch')
        @csrf

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New  Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    Update Password
                </button>
            </div>
        </div>
    </form>
    </div>
</center>
@endsection