@extends('layout.main')
@section('header')
    Login
@endsection
@section('content')
<form method="post" action="/" class="col-lg-5">
  @csrf
    <div class="form-group ">
      <label for="exampleInputEmail1">Email</label>
      <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp">
    </div>
    @error('email')
    <div class="alert alert-danger" role="alert">
      <strong>Harap isi Email!</strong>
    </div>
    @enderror
    <div class="form-group ">
      <label for="exampleInputEmail1">Nama Lengkap</label>
      <input type="email" class="form-control" id="exampleInputEmail1" name="name" aria-describedby="emailHelp">
    </div>
    @error('name')
    <div class="alert alert-danger" role="alert">
      <strong>Harap isi Email!</strong>
    </div>
    @enderror
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" name="password" id="exampleInputPassword1">
    </div>
    @error('password')
    <div class="alert alert-danger" role="alert">
      <strong>Harap isi Password!</strong>
    </div>
    @enderror
    <button type="submit" class="btn btn-primary col">Login</button>
    <a href="/daftar" class="btn btn-primary col my-3">Daftar</a>
  </form>
@endsection