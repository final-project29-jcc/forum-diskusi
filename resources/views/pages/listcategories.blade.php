@extends('layout.main')
@section('header')
  List of Categories
@endsection
@section('content')
<div class="table-responsive">
<table id="example1" class="table table-bordered table-hover">
            @forelse($categories as $key => $category)
                  <tr class="border ">
                    <td class="col-lg-7 align-middle align-center border-0">
                      <a href="/categories/{{ $category->id }}">
                        <h4>{{ $category->category}}</h4>
                      </a>
                      <p>{{ $category->description }}</p>
                    </td>
                    
                    <td class="col-lg-5 align-middle align-center border-0">
                    <p><strong>{{ $question->where('categories_id', $category->id)->count() }}</strong> Pertanyaan</p>
                      
                      <p>Dibuat oleh: <strong>{{ $category->user->name }}</strong></p>

                    </td>
                  @empty
                  No data yet
            @endforelse
</table>

<div align="center">{{ $categories->links() }}</div>
</div>
@endsection

