@extends('layout.main')

@section('header')
    Submit Kategori
@endsection


@section('content')
    <form method="POST" action="/categories">
    @csrf

    <div class="form-group">
        <label>Nama Kategori <span style="color:red">*</span></label>
        <input type="hidden" value="{{ Auth::user()->id}}" name="users_id">
        <input type="text" class="form-control" placeholder="Masukkan judul pertanyaan" name="name" required>
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea class="form-control" rows="5" name="description"></textarea>
    </div>
    <div class="form-group">
        
        <input type="submit" class="btn btn-primary">
    </div>

    </form>
@endsection