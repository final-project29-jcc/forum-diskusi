# KELOMPOK 22
Anggota kelompok: <br>
<ul>
    <li>Apep Wahyudin (@sebuahsemangat)</li>
    <li>Muh Hilman Solehudin (@mannn31)</li>
    <li>Sulthan Alif Hayatyo (@sulthanalif45)</li>
</ul>
<br>

# TEMA & DESKRIPSI

<p>Kelompok kami membuat sebuah Forum Diskusi berbasis Web yang di mana user dapat bertanya dan/atau menjawab pertanyaan dari user lain.</p>

# FITUR
fitur utama: <br>
<ol>
    <li>User harus terdaftar di web untuk menggunakan layanan Forum</li>
    <li>Satu kategori dapat memiliki banyak pertanyaan.</li>
    <li>User dapat menghapus, mengedit, dan menambah kategori</li>
    <li>User dapat membuat pertanyaan berupa: tulisan, gambar, dan kategorinya.</li>
    <li>User dapat membuat, mengedit, dan menghapus pada pertayaan milik sendiri.</li>
    <li>Seorang User dapat memberi jawaban dari pertayaan User lainnya. Sebuah pentayaan dapat memiliki banyak jawaban dari user yang berbeda .</li>
    <li>User dapat membuat, menghapus dan mengedit jawaban milik sendiri</li>
    <li>Seorang User dapat mengubah profile nya sendiri.</li>
    <li>Pada halaman profile terdapat biodata, email, umur, dan alamat</li>
    <li>Library/Package  : Sweet Alert, TinyMCE, FontAwesome, Sweet Talk.</li>
</ol>

# ERD
<img src="public/img/erd/ERD.png">

# Link Video
Link Demo Aplikasi : https://youtu.be/kqZ1ZLy-yWo <br>
Link Deploy : http://kel22-final-project.herokuapp.com/