<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = 'jawabans';
    protected $fillable = ["content", "date", "time", "users_id", "questions_id"];

    public function question () {
        return $this->belongsTo('App\Question', 'questions_id');
    }

    public function user (){
        return $this->belongsTo('App\User', 'users_id');
    }
}
