<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Profile;
use App\User;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('users_id', Auth::id())->first();
        return view('pages.profile', compact('profile'),[
            'user' => Auth::user(),
            'page' => 'Profile'
        ]);
    }

    public function upload(Request $request){
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
  
        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('img/profile_picture'), $imageName);
        $profile = Profile::where('users_id', Auth::id())->update([
            'foto' => $imageName,
        ]);
        
        return redirect()->route('profile')->with('toast_success','You photos has been changed!');
    }

    public function update(Request $request)
    {  
        $user = User::where('id', Auth::id())->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request['password']),
        ]);
        Profile::where('users_id', Auth::id())->update([
            'umur' => $request->umur,
            'bio' => $request->bio,
            'alamat' => $request->alamat,
        ]);

    return redirect()->route('profile')->with('toast_success', 'You profile has been updated!');
    }



}
