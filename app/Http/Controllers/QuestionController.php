<?php

namespace App\Http\Controllers;
use DB;
use App\category;
use App\Question;
use App\Jawaban;
use App\User;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index(){
        $question = Question::all();
        return view('pages.', compact('question'), [
            'page' => 'Question'
        ]);
    }
    
    public function create(){
        $categories = category::all();//select nama-nama kategori
        return view('pages.form-pertanyaan', compact('categories'),[
            'page' => 'Submit Pertanyaan'
        ]);
    }
    public function store(Request $request){
        $request->validate([
            "judul" => 'required',
            "content" => 'required',
        ]);
        $query = DB::table('questions')->insert ([
            "judul" => $request["judul"],
            "content" => $request["content"],
            "date" => date('Y-m-d'),
            "time" => date('H:i'),
            "users_id" => $request["users_id"],
            "categories_id" => $request["categories_id"]
        ]);
        return redirect('/categories/'.$request->categories_id)->with('toast_success','Question has been added!');
    }

    public function show($id){
        //$question = DB::table('questions')->where('id', $id)->first();//first untuk menampilkan satu data
        $question = Question::find($id);
        return view ('pages.question', compact('question'),
    [
        'page' => $question->judul,
        'questions_id' => $id
    ]);
    }

    public function edit($id){
        $question = Question::find($id);
        $categories = category::all();
        $pilihan = category::where('id', $question->categories_id)->first();
        return view('pages.form-editpertanyaan', compact('question'),[
            'page' => 'Edit',
            'categories' => $categories,
            'pilihan' => $pilihan
        ]);
    }

    public function update(Request $request){
        $update = Question::find($request->id)->update([
            'judul' => $request->judul,
            'content' => $request->content,
            'categories_id' => $request->categories_id,
        ]);

        return redirect('/categories/'.$request->categories_id)->with('toast_success','Question has been updated!');
    }

    public function drop($id){
        Question::find($id)->delete();

        return redirect('/list')->with('toast_success','Question has been deleted!');
    }
}
