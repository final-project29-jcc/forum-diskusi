<?php

namespace App\Http\Controllers;
use App\Jawaban;
use Illuminate\Http\Request;

class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jawaban = new Jawaban;
        $jawaban->content = $request->content;
        $jawaban->date = date('Y-m-d');
        $jawaban->time = date('H:i');
        $jawaban->questions_id = $request->questions_id;
        $jawaban->users_id = $request->users_id;
        $jawaban->save();

        return redirect('/question/show/'.$request->questions_id)->with('toast_success','Jawaban has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Jawaban $jawaban)
    {
        $jawaban->update($request->all());
        $update = Jawaban::find($request->id)->update([
            'content' => $request->content,
        ]);

        return redirect('/question/show/'.$request->questions_id)->with('toast_success','Jawaban has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        Jawaban::find($id)->delete();

        return redirect('/question/show/'.$request->questions_id)->with('toast_success','Jawaban has been deleted!');
    }
}
