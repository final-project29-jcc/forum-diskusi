<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\category;
use App\Profile;
use App\Question;
use Illuminate\Support\Facades\Auth;
use App\User as UserInfo;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = category::paginate(5);
        $question = Question::paginate(5);
        return view('pages.home', compact('categories', 'question'),[
            'page' => 'Home'
        ]);
    }
    public function kategori(){
        $categories = category::paginate(5);
        $question = Question::paginate(5);
        return view ('pages.listcategories', compact(['categories', 'question']),
        [
            'page' => 'List of Categories'
        ]);
    }
    
    public function myKategori($id){
        $categories = category::where('users_id', $id)->paginate(5);
        return view('pages.listmycategories', compact('categories'),[
            'page' => 'My Categories'
        ]);
    }

    
}
