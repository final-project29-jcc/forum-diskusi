<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{
    public function index(){
        return view('pages.form-login');
    }

    public function login()
    {
        if (Auth::check()) {
            return redirect('/');
        }else{
            return view('pages.login');
        }
    }
}
