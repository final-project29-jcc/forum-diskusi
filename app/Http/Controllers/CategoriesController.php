<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use App\category;
use App\Question;
use App\Jawaban;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index($id) {
        $categories = category::find($id);
        $jawaban = Jawaban::get();
        $questions = Question::where('categories_id', $id)->paginate(5);
        return view ('pages.categories',compact(['questions', 'jawaban']), [
            'page' => $categories->category
        ]);
    }

    public function create() {
        return view ('pages.form-kategori',[
            'page' => 'Submit Kategori'
        ]);
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            "name" => 'required',
            "description" => 'required',
        ]);
        $query = DB::table('categories')->insert ([
            "category" => $request["name"],
            "description" => $request["description"],
            "users_id" => $request["users_id"]
        ]);
        return redirect('/list/'. $request->users_id)->with('toast_success','Category has been added!');

    }
    public function seacrh(Request $request) {
        $keyword = $request->search;
        $questions = Question::where('judul', 'like', "%" . $keyword . "%")->paginate(5); //mencari data berdasarkan $keyword
        return view ('pages.search',compact('questions'), [
            'page' => 'Hasil Pencarian | '. $keyword
        ]);
    }


    public function edit($id){
        $categories = category::find($id);
        return view('pages.form-editcategories', compact('categories'),[
            'page' => 'Edit Category'
        ]);
    }

    public function update(Request $request){
        $update = category::find($request->id)->update([
            'category' => $request->category,
            'description' => $request->description,
        ]);

        return redirect('/list/'.$request->users_id)->with('toast_success','Category has been updated!');
    }

    public function drop($id){
        $category = category::find($id)->delete();

        return redirect('/list/'.Auth::user()->id);
    }
}
