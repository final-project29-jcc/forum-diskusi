<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table = "categories";
    protected $fillable = ["category", "description", "users_id"];
    public function user (){
        return $this->belongsTo('App\User', 'users_id');
    }
    public function question(){
        return $this->belongsTo('App\Question', 'categories_id');
    }
}
