<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profile";
    protected $primary  = "id";
    protected $fillable = ["umur", "alamat", "bio", "foto", "users_id"];
}
