<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "questions";
    protected $fillable = ["judul", "content", "date", "time", "users_id", "categories_id"];
    
    public function user (){
        return $this->belongsTo('App\User', 'users_id');
    }
    public function category(){
        return $this->belongsTo('App\category', 'categories_id');
    }
    public function jawaban(){
        return $this->hasMany('App\Jawaban', 'questions_id');
    }
    
}
