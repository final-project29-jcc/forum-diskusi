<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route khusus yang sudah login
Route::group(['middleware' => ['auth']], function () {
    
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/list', 'HomeController@kategori');
    Route::get('/list/{id}', 'HomeController@myKategori');
    Route::get('/categories/{id}', 'CategoriesController@index')->name('categories.list');
    //Route::get('/question', 'QuestionController@index');
    //edit kategori
    Route::get('/categories/edit/{id}', 'CategoriesController@edit')->name('categories.edit');
    Route::post('/categories/update', 'CategoriesController@update')->name('categories.update');

    //cari
    Route::get('/search', 'CategoriesController@seacrh')->name('seacrh');
    //view question
    Route::get('/question/show/{id}', 'QuestionController@show');

    //profile
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::patch('/profile/upload', 'ProfileController@upload')->name('upload');
    Route::patch('/profile/update', 'ProfileController@update')->name('update');
    //ubah password
    Route::get('password', 'PasswordController@edit')->name('user.password.edit');
    Route::patch('password', 'PasswordController@update')->name('user.password.update');

    //tambah kategori
    Route::get('/categories/create', 'CategoriesController@create');
    //simpan kategori
    Route::post('/categories/store', 'CategoriesController@store');
    //hapus kategori
    Route::delete('/categories/delete/{id}', 'CategoriesController@drop')->name('categories.destroy');

    //tambah pertanyaan
    Route::get('/question/create/', 'QuestionController@create')->name('question.create');
    //edit pertanyaan
    Route::get('/question/edit/{id}', 'QuestionController@edit')->name('question.edit');
    Route::post('/question/update', 'QuestionController@update')->name('question.update');
    //hapus pertanyaan
    Route::delete('/question/delete/{id}', 'QuestionController@drop')->name('question.destroy');
    //simpan question
    Route::post('question', 'QuestionController@store');
    
    //simpan jawaban
    Route::post('jawaban', 'JawabanController@store');
    //update jawaban
    Route::post('/jawaban/update/{id}', 'JawabanController@update');
    //delete jawaban
    Route::delete('/jawaban/delete/{id}', 'JawabanController@destroy')->name('jawaban.destroy');
});

//login
Route::get('/login', 'LoginController@index');
//Route::post('/', 'LoginController@login')->name('login');

//daftar
Route::get('/register', 'RegisterController@index')->name('register');

//tambah pertanyaan

Auth::routes();

